package com.ays.mesbackbone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MesBackboneApplication {

    public static void main(String[] args) {
        SpringApplication.run(MesBackboneApplication.class, args);
    }

}
