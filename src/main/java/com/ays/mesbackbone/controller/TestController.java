package com.ays.mesbackbone.controller;

import com.ays.mesbackbone.common.distributed.id.DistributedIdGenerator;
import com.ays.mesbackbone.common.distributed.id.SimpleSnowflakeIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title： TestController </p>
 * <p>Description： </p>
 *
 * @author sujunxuan
 * @version V1.0
 * @date 2020/8/4 11:34
 */

@RestController
@RequestMapping("/api/test")
public class TestController {

    @Autowired
    private DistributedIdGenerator idGenerator;

    @Autowired
    private SimpleSnowflakeIdGenerator snowflakeIdGenerator;

    @GetMapping("/id")
    public long id() {
        return idGenerator.generatorId();
    }

    @GetMapping("/id2")
    public long id2() {
        return snowflakeIdGenerator.generatorId();
    }
}
