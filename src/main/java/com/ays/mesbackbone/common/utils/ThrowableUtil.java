package com.ays.mesbackbone.common.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

@SuppressWarnings("all")
public class ThrowableUtil {

    public static String getStackTrace(Throwable throwable){
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }
}
