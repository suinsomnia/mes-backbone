package com.ays.mesbackbone.common.distributed.id;

import org.springframework.stereotype.Component;

/**
 * <p>Title： RedisSnowflakeIdGenerator </p>
 * <p>Description： </p>
 *
 * @author sujunxuan
 * @version V1.0
 * @date 2020/8/4 10:04
 */

@Component
public class RedisSnowflakeIdGenerator implements DistributedIdGenerator {

    @Override
    public long generatorId() {
        // Todo
        return 0;
    }
}
