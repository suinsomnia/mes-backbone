package com.ays.mesbackbone.common.distributed.id;

/**
 * <p>Title： IdGenerator </p>
 * <p>Description： </p>
 *
 * @author sujunxuan
 * @version V1.0
 * @date 2020/8/4 9:58
 */

public interface DistributedIdGenerator {

    /**
     * 生成Id
     * @return id
     */
    long generatorId();
}
