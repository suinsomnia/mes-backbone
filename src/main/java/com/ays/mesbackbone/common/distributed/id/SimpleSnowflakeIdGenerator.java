package com.ays.mesbackbone.common.distributed.id;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>Title： SimpleSnowflakeIdGenerator </p>
 * <p>Description： </p>
 *
 * @author sujunxuan
 * @version V1.0
 * @date 2020/8/4 10:08
 */

@Component
public class SimpleSnowflakeIdGenerator implements DistributedIdGenerator {

    private final SnowflakeWorker snowflakeWorker;

    @Value("${data-center-id:0}")
    private long dataCenterId;

    @Value("${machine-id:0}")
    private long machineId;

    public SimpleSnowflakeIdGenerator() {
        snowflakeWorker = new SnowflakeWorker(dataCenterId, machineId);
    }

    @Override
    public long generatorId() {
        return snowflakeWorker.nextId();
    }
}
